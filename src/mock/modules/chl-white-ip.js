import Mock from 'mockjs'

// 生成数据列表
var dataList = []
for (let i = 0; i < Math.floor(Math.random() * 10 + 1); i++) {
  dataList.push(Mock.mock({
    'id': '@id',
    'ip': '@ip',
    'remark': '@remark',
    'status': 1,
    'createTime': 'datetime'
  }))
}

// 获取白名单列表
export function list () {
  return {
    // isOpen: false,
    url: 'agency/white/list',
    type: 'get',
    data: {
      'msg': 'success',
      'code': 0,
      'page': {
        'totalCount': dataList.length,
        'pageSize': 10,
        'totalPage': 1,
        'currPage': 1,
        'list': dataList
      }
    }
  }
}

// 获取白名单信息
export function info () {
  return {
    // isOpen: false,
    url: '/agency/white/info',
    type: 'get',
    data: {
      'msg': 'success',
      'code': 0,
      'user': dataList[0]
    }
  }
}

// 修改白名单信息
export function updateWhiteIp () {
  return {
    // isOpen: false,
    url: '/agency/white/update',
    type: 'post',
    data: {
      'msg': 'success',
      'code': 0
    }
  }
}

// 添加白名单信息
export function addWhiteIp () {
  return {
    // isOpen: false,
    url: '/agency/white/save',
    type: 'post',
    data: {
      'msg': 'success',
      'code': 0
    }
  }
}

// 修改白名单信息
export function update () {
  return {
    // isOpen: false,
    url: '/agency/white/update',
    type: 'post',
    data: {
      'msg': 'success',
      'code': 0
    }
  }
}

// 删除白名单信息
export function del () {
  return {
    // isOpen: false,
    url: '/agency/white/delete',
    type: 'post',
    data: {
      'msg': 'success',
      'code': 0
    }
  }
}
