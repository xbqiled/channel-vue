import Mock from 'mockjs'
import * as common from './modules/common'
import * as jobSchedule from './modules/job-schedule'
import * as oss from './modules/oss'
import * as chlConfig from './modules/chl-config'
import * as chlLog from './modules/chl-log'
import * as chlMenu from './modules/chl-menu'
import * as chlRole from './modules/chl-role'
import * as chlWhiteIp from './modules/chl-white-ip'
import * as chlUser from './modules/chl-user'

// tips
// 1. 开启/关闭[业务模块]拦截, 通过调用fnCreate方法[isOpen参数]设置.
// 2. 开启/关闭[业务模块中某个请求]拦截, 通过函数返回对象中的[isOpen属性]设置.
fnCreate(common, false)
fnCreate(jobSchedule, false)
fnCreate(oss, false)
fnCreate(chlConfig, false)
fnCreate(chlLog, false)
fnCreate(chlMenu, false)
fnCreate(chlRole, false)
fnCreate(chlUser, false)
/** 登录白名单**/
fnCreate(chlWhiteIp, false)

/**
 * 创建mock模拟数据
 * @param {*} mod 模块
 * @param {*} isOpen 是否开启?
 */
function fnCreate (mod, isOpen = true) {
  if (isOpen) {
    for (var key in mod) {
      ((res) => {
        if (res.isOpen !== false) {
          Mock.mock(new RegExp(res.url), res.type, (opts) => {
            opts['data'] = opts.body ? JSON.parse(opts.body) : null
            delete opts.body
            console.log('\n')
            console.log('%cmock拦截, 请求: ', 'color:blue', opts)
            console.log('%cmock拦截, 响应: ', 'color:blue', res.data)
            return res.data
          })
        }
      })(mod[key]() || {})
    }
  }
}
